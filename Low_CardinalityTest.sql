
# Low cardinality field sweepstakes testing in MYSQL
-- use the high cardinality fields listed in the sweeps testing document to replace the 
field in the select statement, count function and the group by for each test.
-- Adjust where statement for appropriate sweepstakes name and load date. 


Select ip_address, count(ip_address)
from sweeps_registration
where sweepstakes_name='DIY NETWORK BLOG CABIN 2014'
and date(LOAD_DATE)=current_date
group by ip_address;



# Low cardinality field sweepstakes testing in Oracle SQL Developer

select BIRTH_MONTH, count(BIRTH_MONTH)
from stage.wildfire_registration
where SWEEPSTAKES_NAME='DIY NETWORK BLOG CABIN 2014'
AND TRUNC(load_date)='13-AUG-14'
group by birth_month;